# iframe

## 一、配置

- 图层名称：组件的名称。（建议设置，方便后期组件管理）

- 地址：设置为 iframe 的页面地址，例如设为“https://www.baidu.com/”，如下图；

  <viewer>
  <img src="../image/iframe-1.png"width="60%">
  </viewer>

- 背景颜色：iframe 页面背景颜色；

- 载入动画：组件的载入动画效果。分别为：弹跳、渐隐渐显、向右滑入、向左滑入、放缩、旋转、滚动。
