# 旭日图

### 1、图层

选中该旭日图组件，在操作界面右侧的“图层名称”处修改该组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；

### 2、标题

选中该旭日图组件，在操作界面右侧的“标题”处可修改该组件的标题。

- 标题：标题显示的内容，如果不想显示标题，不填写内容就不显示；

- 副标题：副标题内容，如果不想显示副标题，不填写内容就不显示；

- 标题位置：单选，分为「居左」「居中」「居右」，如下图；

- 标题字号：标题文字的字体大小；

- 标题粗细：标题文字字体的粗细，可选：
  > * 'normal'：正常
  >
  > * 'bold'：粗体
  >
  > * 'bolder'：加粗体
  >
  > * 'lighter'：较轻

- 字体样式：标题文字的字体系列；

  <viewer>
  <img src="../image/sunburst1.png" width="60%">
  </viewer>

### 3、图形设置

- 半径大小：最内圈圆环半径大小，如下图所示半径分别为20,50；
  <viewer>
  <img src="../image/sunburst2.png" width="100%">
  </viewer>

- 边框宽度：圆环边框宽度，如下图所示边框宽度分别为2,7；
  <viewer>
  <img src="../image/sunburst3.png" width="100%">
  </viewer>

- 排序方式：数据排序方式，分为不排序、升序、降序，如下图所示分别为降序、升序方式；
  <viewer>
  <img src="../image/sunburst4.png" width="100%">
  </viewer>

### 4、标签设置

- 标签显示开关：是否显示图形上标签文字，如下图；
  <viewer>
  <img src="../image/sunburst5.png" width="100%">
  </viewer>

- 旋转角度：标签文字旋转角度，分为径向旋转、切向旋转；

- 字体大小：标签文字字体大小，如下图所示分别为12,20；

- 字体颜色：标签文字字体颜色，如下图所示分别为白色，黄色；
  <viewer>
  <img src="../image/sunburst6.png" width="100%">
  </viewer>

- 载入动画：弹跳、渐隐渐现、向右滑入、向左滑入、放缩、旋转、滚动；

## 二、数据

静态数据格式：

```
[
  {"name":"Grandpa","children":[
    {"name":"Uncle Leo","value":15,"children":[
      {"name":"Cousin Jack","value":2},
      {"name":"Cousin Mary","value":5,"children":[
        {"name":"Jackson","value":2}
      ]},
      {"name":"Cousin Ben","value":4}]},
      {"name":"Father","value":10,"children":[
        {"name":"Me","value":5},
        {"name":"Brother Peter","value":1}
      ]}
    ]},
    {"name":"Nancy","children":[
      {"name":"Uncle Nike","children":[
        {"name":"Cousin Betty","value":1},
        {"name":"Cousin Jenny","value":2}
      ]}
    ]}
]
```

