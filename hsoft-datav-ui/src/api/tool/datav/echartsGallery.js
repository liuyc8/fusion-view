import request from '@/utils/request'

// 查询echarts素材列表
export function listGallery(query) {
  return request({
    url: '/datav/echartsGallery/list',
    method: 'get',
    params: query
  })
}

// 查询echarts素材详细
export function getGallery(id) {
  return request({
    url: '/datav/echartsGallery/' + id,
    method: 'get'
  })
}

// 新增echarts素材
export function addGallery(data) {
  return request({
    url: '/datav/echartsGallery',
    method: 'post',
    data: data
  })
}

// 修改echarts素材
export function updateGallery(data) {
  return request({
    url: '/datav/echartsGallery',
    method: 'put',
    data: data
  })
}

// 删除echarts素材
export function delGallery(data) {
  return request({
    url: '/datav/echartsGallery',
    method: 'delete',
    data:data
  })
}

// 导出echarts素材
export function exportGallery(query) {
  return request({
    url: '/datav/echartsGallery/export',
    method: 'get',
    params: query
  })
}


// 查看数量+1
export function viewIncrease(data) {
  return request({
    url: '/datav/echartsGallery/viewIncrease',
    method: 'post',
    data: data
  })
}

// 收藏数量+1
export function starIncrease(data) {
  return request({
    url: '/datav/echartsGallery/starIncrease',
    method: 'post',
    data: data
  })
}

// 收藏数量-1
export function starDecrease(data) {
  return request({
    url: '/datav/echartsGallery/starDecrease',
    method: 'post',
    data: data
  })
}
